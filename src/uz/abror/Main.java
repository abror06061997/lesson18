package uz.abror;

public class Main {

    public static void main(String[] args) {
        Operationable operationable = (x, y, z) -> (x + y) > z;
        System.out.println(operationable.calculate(6, 8, 14) ? "Teng" : "Teng emas");

        if (operationable.calculate(6, 8, 14))
            System.out.println("teng");
        else
            System.out.println("teng emas");
    }
}

