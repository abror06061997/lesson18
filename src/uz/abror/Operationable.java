package uz.abror;

public interface Operationable {
    boolean calculate(int x, int y, int z);
}
